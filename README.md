Change Log
==========

Version 1.0.3 *(2019-03-31)*
----------------------------

 * UI test with espresso added
 
 
Version 1.0.3 *(2019-03-31)*
----------------------------

 * Moving initialize processes into AsyncTask to avoid stoping UI thread in App starting. (SearchModel.Java) 
 * Create icon for app

Version 1.0.2 *(2019-03-31)*
----------------------------

 * I'v created "createIndexHashMap" function in "PublicMethods" for create a Hashmap for indexing cities.  (SearchModel.Java) 
 * At the first, am iterate all cities list and add first char of each city to that HashMap, if any character is exist in Hashmap,
 * I will update previous record and change index of char, for example :  
 Galaxy J5 with Android 6  
 0- Alabama    Hashmap ==>   key : a  , value : firstIndex:0 , lastIndex: 0  
 1- Alabamas   Hashmap ==>   key : a  , value : firstIndex:0 , lastIndex: 1  
 2- Alabamasa  Hashmap ==>   key : a  , value : firstIndex:0 , lastIndex: 2  
 3- Tehran     Hashmap ==>   key : t  , value : firstIndex:3 , lastIndex: 3  
 4- Tokyo      Hashmap ==>   key : t  , value : firstIndex:3 , lastIndex: 4  
 
 * With this method, for each character searching, i should iterate a much more smaller list instead of all cities list,
 * I received this results with new solution. it seems it is 20 times faster than previous way.
 
 * a : 47 milliSeconds
 * am : 49 milliSeconds
 * ams : 44 milliSeconds
 * amst : 47 milliSeconds
 * amste : 46 milliSeconds
 * amster : 45 milliSeconds
 * amsterd : 45 milliSeconds
 * amsterda : 48 milliSeconds
 * amsterdam : 47 milliSeconds
 

Version 1.0.1 *(2019-03-30)*
----------------------------

 * Requirements done, i am going to start improve search optimization.
 * i am using GSON for parse json and sort and store parsed data to a static variable inside MyApplication.
 * big challenge is decrease search time, in traditional search solution i received this results (Iterate in ListArray)  :
 all benchmarks tested on Galaxy J5 with Android 6
 
 * a : 786 milliSeconds  
 * am : 769 milliSeconds
 * ams : 773 milliSeconds
 * amst : 766 milliSeconds
 * amster : 769 milliSeconds
 * amsterda : 761 milliSeconds
 * amsterdam : 756 milliSeconds

Version 1.0.1 *(2019-03-30)*
----------------------------

 * 2 fragment added to project. 2 layer, one for landscape and the other one for portrait mode created for MainActivity.
 * SearchFragment included by AutoCompleteTextView for searching and MapFragment created for showing map.
 * MainActivity contains that 2 fragments and also handle change orientations.
 * I am using MVP for this small sample but for bigger projects i prefer to use RX Live data and MVVM.

Version 1.0.1 *(2019-03-29)*
----------------------------

 * implementation: MVP created. Base classes and files created


Version 1.0.0 *(2019-03-29)*
----------------------------

Initial release.


