package backbase.map.ui.map;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import backbase.map.R;
import backbase.map.utils.CityPOJO;
import backbase.map.utils.Configs;
import backbase.map.utils.PublicMethods;


public class MapFragment extends Fragment implements OnMapReadyCallback {
    CityPOJO cityToFocus;
    private static MapFragment mapFragment;
    private static GoogleMap map;
    private Marker addedMarker;

    public static MapFragment getInstance() {
        if (mapFragment == null)
            mapFragment = new MapFragment();
        return mapFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        return view;
    }

    public void setArgs(Bundle bundle) {
        if (bundle != null) {
            cityToFocus = (CityPOJO) bundle.getSerializable(Configs.CITY_DATA_KEY);
            focusOnCity();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        PublicMethods.setMapUISettings(map);
        focusOnCity();
    }

    private void focusOnCity() {
        if (cityToFocus != null && map != null) {
            LatLng coordinate = new LatLng(cityToFocus.getCoord().getLat(), cityToFocus.getCoord().getLon());
            CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                    coordinate, Configs.ZOOM_LEVEL);
            map.animateCamera(location);

            //remove previous marker if exist
            if (addedMarker != null)
                addedMarker.remove();

            //add new marker
            addedMarker = map.addMarker(new MarkerOptions()
                    .position(coordinate)
                    .title(cityToFocus.getName())
                    .icon(BitmapDescriptorFactory
                            .defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)));
        }
    }
}
