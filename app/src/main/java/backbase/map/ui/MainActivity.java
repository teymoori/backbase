package backbase.map.ui;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import backbase.map.R;
import backbase.map.ui.map.MapFragment;
import backbase.map.utils.CityPOJO;
import backbase.map.utils.Configs;
import backbase.map.utils.PublicMethods;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void showOnMap(CityPOJO city) {
        MapFragment initialFragment = MapFragment.getInstance();
        Bundle args = new Bundle();
        args.putSerializable(Configs.CITY_DATA_KEY, city);
        initialFragment.setArgs(args);

        if (PublicMethods.getScreenOrientation(this) == Configs.SCREEN_ORIENTATION.PORTRAIT) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, initialFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }

}
