package backbase.map.ui.search;

import java.util.List;

import backbase.map.utils.CityPOJO;

public interface SearchContract {

    interface View {
        void onCitiesLoaded(List<CityPOJO> cities);
        void onChangeProcess(String process) ;
    }

    interface Presenter {
        void setView(View view) ;
        void onViewCreated();
        void onViewDestroyed();
        void onCitiesLoaded(List<CityPOJO> cities);
        void onChangeProcess(String process) ;
    }

    interface Model {
        void setPresenter(Presenter presenter) ;
        void loadCities();
    }
}
