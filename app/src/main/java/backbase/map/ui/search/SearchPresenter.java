package backbase.map.ui.search;

import java.lang.ref.WeakReference;
import java.util.List;

import backbase.map.utils.CityPOJO;

public class SearchPresenter implements SearchContract.Presenter {


    private SearchContract.View view;
    private SearchContract.Model model = new SearchModel();


    @Override
    public void setView(SearchContract.View view) {
        this.view = view;
        model.setPresenter(this);
    }

    @Override
    public void onViewCreated() {
        model.loadCities();
    }

    @Override
    public void onViewDestroyed() {
        this.view = null;
    }

    @Override
    public void onCitiesLoaded(List<CityPOJO> cities) {
        if (this.view != null)
            view.onCitiesLoaded(cities);
    }

    @Override
    public void onChangeProcess(String process) {
        view.onChangeProcess(process);
    }
}
