package backbase.map.ui.search;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import backbase.map.R;
import backbase.map.ui.MainActivity;
import backbase.map.utils.CityPOJO;
import backbase.map.utils.Configs;
import backbase.map.utils.PublicMethods;

public class SearchFragment extends Fragment implements SearchContract.View, SearchAdapter.ResultsCount, TextWatcher {
    AutoCompleteTextView location;
    SearchContract.Presenter presenter = new SearchPresenter();
    View progressbar;
    TextView processTV, countTV;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        location = view.findViewById(R.id.location);
        location.addTextChangedListener(this);
        processTV = view.findViewById(R.id.process);
        countTV = view.findViewById(R.id.count);
        progressbar = view.findViewById(R.id.progressbar);
        location.setVisibility(View.INVISIBLE);
        location.setOnItemClickListener((parent, view1, position, id) -> {
            CityPOJO selected = ((CityPOJO) parent.getAdapter().getItem(position));
            ((MainActivity) getActivity()).showOnMap(selected);
            PublicMethods.hideKeyboard(getActivity());
            location.setText(selected.getName());
        });
        presenter.setView(this);
        presenter.onViewCreated();
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onViewDestroyed();
    }

    @Override
    public void onCitiesLoaded(List<CityPOJO> cities) {
        progressbar.setVisibility(View.INVISIBLE);
        location.setVisibility(View.VISIBLE);
        SearchAdapter adapter = new SearchAdapter(getActivity(), R.layout.fragment_search, cities);
        adapter.resultsCount = this;
        location.setThreshold(Configs.AUTOCOMPLETE_THRESHOLD);
        location.setAdapter(adapter);
    }

    @Override
    public void onChangeProcess(String process) {
        if (getActivity() != null)
            getActivity().runOnUiThread(() -> processTV.setText(process));

    }

    @Override
    public void onResults(int count) {
       // countTV.setText(count + " Result");
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (count == 0) onResults(0);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
