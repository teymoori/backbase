package backbase.map.ui.search;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import backbase.map.R;
import backbase.map.utils.CharIndexModel;
import backbase.map.utils.CityPOJO;
import backbase.map.utils.MyApplication;

public class SearchAdapter extends ArrayAdapter<CityPOJO> {
    private Context mContext;

    private List<CityPOJO> originalData;
    private List<CityPOJO> suggestedData;

    SearchAdapter(Context context, int resource, List<CityPOJO> originalData) {
        super(context, resource);
        this.mContext = context;
        this.originalData = originalData;
        this.suggestedData = originalData;
    }

    @Override
    public int getCount() {
        return suggestedData == null ? 0 : suggestedData.size();
    }

    @Override
    public CityPOJO getItem(int position) {
        return suggestedData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v;
        if (convertView == null)
            v = LayoutInflater.from(mContext).inflate(R.layout.search_result_item, parent, false);
        else
            v = convertView;

        TextView title = v.findViewById(R.id.title);
        TextView subtitle = v.findViewById(R.id.subtitle);
        title.setText(suggestedData.get(position).getTitle());
        subtitle.setText(suggestedData.get(position).getSubTitle());
        return v;
    }

    @Override
    public Filter getFilter() {
        return new LocationFilter();
    }

    class LocationFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
           // resultsCount.onResults(((int) (System.currentTimeMillis())));

            String filterString = constraint.toString().toLowerCase().trim();

            FilterResults results = new FilterResults();
            List<CityPOJO> filtered = new ArrayList<>();

            long beforeProcessStartTime = System.currentTimeMillis();

            CharIndexModel rangeOfFirstChar = MyApplication.indexedCities.get(filterString.substring(0, 1));
            //optimization , using indexed cities in hash map
            List<CityPOJO> citiesRangeByFirstChar = originalData.subList(rangeOfFirstChar.firstIndex, rangeOfFirstChar.lastIndex);

            for (int i = 0; i < citiesRangeByFirstChar.size(); i++) {
                CityPOJO currentCity = citiesRangeByFirstChar.get(i);
                if (currentCity.getName().toLowerCase().startsWith(filterString)) {
                    filtered.add(currentCity);
                }
            }

            results.values = filtered;
            results.count = filtered.size();

            calculateProcessTime(filterString, beforeProcessStartTime);

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            suggestedData = (List<CityPOJO>) results.values;
            notifyDataSetChanged();
        }
    }

    //return results count to View Layer
    ResultsCount resultsCount;

    interface ResultsCount {
        void onResults(int count);
    }


    private void calculateProcessTime(String filterString, long start) {
        long now = System.currentTimeMillis();
        long elapsedTime = now - start;
        Log.d("calculateProcessTime", filterString + " : " + elapsedTime + " milliSeconds");
    }
}
