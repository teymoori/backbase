package backbase.map.ui.search;

import android.content.Context;
import android.os.AsyncTask;

import backbase.map.R;
import backbase.map.utils.Configs;
import backbase.map.utils.MyApplication;
import backbase.map.utils.PublicMethods;

public class SearchModel implements SearchContract.Model {
    private SearchContract.Presenter presenter;

    @Override
    public void
    setPresenter(SearchContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void loadCities() {
        //parse and sorting cities are very consuming task.
        //It's better to save it on static variable in first time for reusing
        if (MyApplication.sortedCities == null) {
            new FirstInitialize().execute();
        } else
            presenter.onCitiesLoaded(MyApplication.sortedCities);
    }

    //first initialize data includes parse json, add to a list, sort list, add to hashmap for indexing
    //it's so huge process, for avoid to stop UI thread, i located that in a AsyncTask
    //each step will send to view layer to show to user

    public void initApp(boolean showProcessToUser, Context mContext) {

        if (showProcessToUser)
            presenter.onChangeProcess(mContext.getString(R.string.loading_assets));
        String js = PublicMethods.loadFromAssets(mContext, Configs.CITIES_DATA_FILE);

        if (showProcessToUser)
            presenter.onChangeProcess(mContext.getString(R.string.sorting));
        MyApplication.sortedCities = PublicMethods.sortedCities(js);
        //create hash map of keys for faster search

        if (showProcessToUser)
            presenter.onChangeProcess(mContext.getString(R.string.indexing));
        PublicMethods.createIndexHashMap(MyApplication.sortedCities);
    }

    private class FirstInitialize extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            initApp(true, MyApplication.myAppContext);
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            presenter.onCitiesLoaded(MyApplication.sortedCities);
        }
    }


}
