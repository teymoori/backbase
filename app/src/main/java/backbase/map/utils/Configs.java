package backbase.map.utils;

public class Configs {
    public static int ZOOM_LEVEL = 13;
    public static int AUTOCOMPLETE_THRESHOLD = 1;
    public static String CITY_DATA_KEY = "city";
    public static String CITIES_DATA_FILE = "cities.json";
    public enum SCREEN_ORIENTATION{
        PORTRAIT , LANDSCAPE
    }
}
