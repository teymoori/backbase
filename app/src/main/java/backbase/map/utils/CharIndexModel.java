package backbase.map.utils;

public class CharIndexModel {
    public int firstIndex,lastIndex ;

    public CharIndexModel(int firstIndex, int lastIndex) {
        this.firstIndex = firstIndex;
        this.lastIndex = lastIndex;
    }

    @Override
    public String toString() {
        return "CharIndexModel{" +
                "firstIndex=" + firstIndex +
                ", lastIndex=" + lastIndex +
                '}';
    }
}
