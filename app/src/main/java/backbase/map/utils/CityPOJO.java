
package backbase.map.utils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class CityPOJO implements Serializable {

    @Expose
    private Long _id;
    @Expose
    private Coord coord;
    @Expose
    private String country;
    @Expose
    private String name;

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return this.getName().concat(" ( ").
                concat(this.getCountry().concat(" )"));
    }

    public String getSubTitle() {
        return "Latitude: " + getCoord().getLat() + " Longitude:" + getCoord().getLon();
    }

}
