package backbase.map.utils;

import android.app.Application;
import android.content.Context;

import java.util.List;
import java.util.Map;

public class MyApplication extends Application {
    public static Context myAppContext;
    public static List<CityPOJO> sortedCities ;
    public static Map<String, CharIndexModel> indexedCities;
    @Override
    public void onCreate() {
        super.onCreate();
        myAppContext = this.getApplicationContext();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
