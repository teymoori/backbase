package backbase.map.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import backbase.map.R;

public class PublicMethods {

    public static String loadFromAssets(Context mContext , String fileAddress) {
        String json = null;
        try {
            InputStream is = mContext.getAssets().open(fileAddress);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static List<CityPOJO> sortedCities(String js) {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<CityPOJO>>() {
        }.getType();
        List<CityPOJO> cities = gson.fromJson(js, listType);
        Collections.sort(cities, (item1, item2) -> {
            String s1 = item1.getName();
            String s2 = item2.getName();
            return s1.compareToIgnoreCase(s2);
        });
        return cities;
    }


    //this method used for search optimization
    //at the app loading time, all cities
    public static void createIndexHashMap(List<CityPOJO> sortedCities) {
        Map<String, CharIndexModel> indexedCities = new HashMap<String, CharIndexModel>();
        int i = 0;
        for (CityPOJO city : sortedCities) {
            String firstChar = city.getName().substring(0, 1).toLowerCase();
            if (indexedCities.get(firstChar) == null) {
                indexedCities.put(firstChar, new CharIndexModel(i, i));
            } else {
                CharIndexModel val = indexedCities.get(firstChar);
                indexedCities.put(firstChar, new CharIndexModel(val.firstIndex, i));
            }
            i++;
        }
        MyApplication.indexedCities = indexedCities;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static Configs.SCREEN_ORIENTATION getScreenOrientation(Activity mActivity) {
        int orientation = mActivity.getResources().getConfiguration().orientation;
        if (Configuration.ORIENTATION_LANDSCAPE == orientation) {
            return Configs.SCREEN_ORIENTATION.LANDSCAPE;
        } else {
            return Configs.SCREEN_ORIENTATION.PORTRAIT;
        }
    }

    public static void setMapUISettings(GoogleMap map) {
        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setAllGesturesEnabled(true);
        map.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                        MyApplication.myAppContext, R.raw.mapstyle));
    }

}