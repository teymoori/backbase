package backbase.map.ui;


import android.app.Activity;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.matcher.RootMatchers;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import backbase.map.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class SearchFragmentTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void searchFragmentTest() {
        Activity mActivity = mActivityTestRule.getActivity();

        ViewInteraction countTV = onView(
                withId(R.id.count));

        ViewInteraction locationACTV = onView(
                withId(R.id.location));

        //wait for initialize
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //if city exist, test should be failed
        locationACTV.perform(click(), typeText("amsterdam"), closeSoftKeyboard());

        //if city not exist, test should pass
        // locationACTV.perform(click(), typeText("asdfghjkl"), closeSoftKeyboard());


        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //  countTV.check(matches(withText("0 Result")));
        locationACTV.inRoot(RootMatchers.isPlatformPopup())
                .perform(click());
    }


}
